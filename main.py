from task_engine import PaintingBoard, Cell


def num_sum(value: int) -> int:
    """
    Возвращает сумму чисел числа
    """
    _sum = 0
    while value != 0:
        _sum += value % 10
        value = value // 10

    return _sum


def find_allowed_cells():
    """
    Функция вычисляет количество клеток которые может посетить муравей

    :return:
    """

    start_x = 1000
    start_y = 1000

    board = PaintingBoard(
        width=699,
        height=699,
        width_offset=start_x,
        height_offset=start_y,
        cell_availability=lambda x, y: (num_sum(x) + num_sum(y)) <= 25,
    )

    cells_to_check = [Cell(x=start_x, y=start_y, board=board)]

    while cells_to_check:
        cell = cells_to_check.pop()
        cell.paint()

        for cell_neighbor in cell.cells_around:
            cell_neighbor: Cell
            if cell_neighbor.is_allowed and not cell_neighbor.is_painted:
                cells_to_check.append(cell_neighbor)

    return board.count_colored_cache


if __name__ == '__main__':
    print(find_allowed_cells())
