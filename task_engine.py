
class PaintingBoard:
    def __init__(self, width: int, height: int, cell_availability: callable, width_offset: int = 0, height_offset: int = 0):
        """
        Доска/Поле с клетками

        :param width: Ширина доски/поля
        :param height: Высота доски/поля
        :param cell_availability: Функция для проверки доступна ли клетка. Должна принимать 2 аргумента: (x, y)
        :param width_offset: Атрибут для оптимизации памяти программы, задает минимальное доступное значение по X
        :param height_offset: Атрибут для оптимизации памяти программы, задает минимальное доступное значение по Y
        """

        self.paint_map = [
            [0 for w in range(width)] for h in range(height)
        ]
        self.cell_availability = cell_availability
        self.x_offset = width_offset
        self.y_offset = height_offset
        self.count_colored_cache = 0

    def invalid_index(self, x: int = None, y: int = None):
        raise IndexError(
                f"Coordinates ({x=},{y=}) is out of board : "
                f"x ∈ [{self.x_offset}, ..., {self.x_offset + len(self.paint_map[0]) - 1}], "
                f"y ∈ [{self.y_offset}, ..., {self.y_offset + len(self.paint_map) - 1}].")

    def real_x(self, x: int):
        """
        Возвращает значение координаты X с учетом оптимизации self.x_offset
        """

        real_val = x - self.x_offset

        if real_val < 0:
            self.invalid_index(x=x)

        return real_val

    def real_y(self, y: int):
        """
        Возвращает значение координаты Y с учетом оптимизации self.y_offset
        """

        real_val = y - self.y_offset

        if real_val < 0:
            self.invalid_index(y=y)

        return real_val

    def paint(self, x: int, y: int):
        """
        Закрасить клетку с координатами (x, y)

        :param x: Координата клетки по x
        :param y:  Координата клетки по y
        """

        try:
            if self.paint_map[self.real_x(x)][self.real_y(y)] == 0:
                self.paint_map[self.real_x(x)][self.real_y(y)] = 1
                self.count_colored_cache += 1

        except IndexError:
            self.invalid_index(x=x, y=y)

    def is_cell_painted(self, x: int, y: int) -> bool:
        """
        Проверка закрашена ли клетка с координатами (x, y)

        :param x: Координата клетки по x
        :param y: Координата клетки по y
        """

        try:
            return bool(self.paint_map[self.real_x(x)][self.real_x(y)])

        except IndexError:
            self.invalid_index(x=x, y=y)

    def is_cell_allowed(self, x: int, y: int) -> bool:
        """
        Проверка доступна ли муравью для посещения клетка с координатами (x, y)

        :param x: Координата клетки по x
        :param y: Координата клетки по y
        """

        return self.cell_availability(x, y)

    def count_painted(self):
        return sum(sum(line) for line in self.paint_map)


class Cell:
    __slots__ = ['x', 'y', 'board']

    def __init__(self, board: PaintingBoard, x: int = 0, y: int = 0):
        """
        Клетка на доске/поле

        :param board: Поле/доска
        :param x: Координата клетки по x
        :param y: Координата клетки по y
        """

        self.x = x
        self.y = y
        self.board = board

    def __str__(self):
        return f"<Cell: ({self.x}, {self.y})>"

    @property
    def is_allowed(self) -> bool:
        """
        Доступна ли клетка муравью
        """
        return self.board.is_cell_allowed(x=self.x, y=self.y)

    @property
    def is_painted(self) -> bool:
        """
        Закрашена ли клетка
        """
        return self.board.is_cell_painted(x=self.x, y=self.y)

    def paint(self):
        """
        Закрасить клетку
        """
        self.board.paint(x=self.x, y=self.y)

    @property
    def cells_around(self) -> tuple:
        """
        Список клеток рядом (сверху, снизу, слева и справа)
        """

        return (
            Cell(x=self.x, y=self.y + 1, board=self.board),
            Cell(x=self.x, y=self.y - 1, board=self.board),
            Cell(x=self.x - 1, y=self.y, board=self.board),
            Cell(x=self.x + 1, y=self.y, board=self.board),
        )
